#include <memory>
#include "App.h"

auto main(int argc, char *argv[]) -> int
{
    auto app = std::make_shared<App>();
    app->Start(argc,argv);
    return 0;
}
