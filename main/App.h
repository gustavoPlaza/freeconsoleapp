//
// Created by kaiser on 12/2/22.
//
#pragma once
#include <iostream>
#include <memory>
#include <boost/core/demangle.hpp>
#include <vector>
#include <algorithm>
#include "menu/Helper.h"
#include "menu/UserManagement/UserMenu.h"
#include "menu/Help/ShowHelp.h"
#include "menu/VideoGameManagement/VideoGameMenu.h"
#include "menu/DatabaseManagement/DatabaseMenu.h"
#include "menu/VideoUserManagement/VideoUserMenu.h"
#include "menu/IMenuType.h"
#include "fmt/core.h"

#include "menu/DatabaseManagement/Structure/User.h"
#include "menu/DatabaseManagement/Structure/VideoGame.h"
#include "menu/DatabaseManagement/Structure/userVideoGame.h"
#include "menu/DatabaseManagement/Structure/DatabaseConnector.h"

class App: public  std::enable_shared_from_this<App> {
public:
    std::vector<std::shared_ptr<IMenuType>> availableTools;
    std::shared_ptr<DatabaseConnector> connector;
    explicit App()= default;;

    ~App()= default;;


    void loadAvailableTools()
    {
        availableTools.emplace_back(std::make_shared<UserMenu>());
        availableTools.back()->LoadAvailableCommands();
        availableTools.emplace_back(std::make_shared<VideoGameMenu>());
        availableTools.back()->LoadAvailableCommands();
        availableTools.emplace_back(std::make_shared<DatabaseMenu>());
        availableTools.back()->LoadAvailableCommands();
        availableTools.emplace_back(std::make_shared<ShowHelp>());
        availableTools.back()->LoadAvailableCommands();
        availableTools.emplace_back(std::make_shared<VideoUserMenu>());
        availableTools.back()->LoadAvailableCommands();

        fmt::print(fmt::format("{:60}\n", "Available options are:"));
        for (auto &menu: availableTools) {
            menu->showInformation();
        }
    }

    void RunAsSingleCommand(std::vector<std::string> args)
    {
        try
        {
            std::vector<std::shared_ptr<IMenuType>> commandsToExecute;
            int position;
            bool toolFound = false;
            for (position = 0; position < args.size(); position++)
            {
                std::string command = args[position];
                auto tool = std::find_if(availableTools.begin(), availableTools.end(), [&command](std::shared_ptr<IMenuType> const & obj) ->bool{ return obj->command == command; });

                if (tool != availableTools.end())
                {
                    args.erase(args.begin());
                    toolFound = true;
                    position += (*tool)->doCommand(args, shared_from_this());
                }
                if(!args.empty()) {
                    args.erase(args.begin());
                }
            }
            if (!toolFound)
            {
                std::cout << "Not found .. " << std::endl;
            }
        }catch( const std::exception & ex)
        {
            std::cout << "Error .. " <<  ex.what() << std::endl;
        }
    }


    void RunCommand(const std::string& command)
    {
        auto args = Helper::splitString(command, " ");
        args.erase(std::remove_if(args.begin(), args.end(), [](const std::string &s){return s.empty();}), args.end());
        RunAsSingleCommand(args);
    }
    void ShowAvailableTools()
    {
        for(const auto&  t : availableTools)
        {
            std::cout << t->description << std::endl;
        }
    }

    void Start(int argc, char *argv[]){

        fmt::print(fmt::format("{:*^60}\n", "*"));
        fmt::print(fmt::format("{:*^60}\n", "  Menu  "));
        fmt::print(fmt::format("{:*^60}\n", "*"));

        loadAvailableTools();

        if (argc > 1)
        {

            std::vector<std::string> allArgs(argv + 1, argv + argc);
            std::ostringstream s;
            copy(allArgs.begin(),allArgs.end(), std::ostream_iterator<std::string>(s,std::string(" ").c_str()));
            allArgs = Helper::splitString(s.str(), ";");
            for (const auto& arg :allArgs) {
                auto args = Helper::splitString(arg, " ");
                args.erase(std::remove_if(args.begin(), args.end(), [](const std::string &s){return s.empty();}), args.end());
                RunAsSingleCommand(args);
            }
        }


        if (argc == 1)
        {
            fmt::print(fmt::format("{:<60}\n", "Write \"<option> help\" (\"users help\") for option help.\n"));
            fmt::print(fmt::format("{:<60}\n", "Write \"help\", or quit for exit: \n"));
            std::string readLine;
            std::getline(std::cin, readLine);
            while (readLine != "exit"
                   && readLine != "quit")
            {
                RunCommand(readLine);
                std::getline(std::cin, readLine);
            }
        }
    }
};