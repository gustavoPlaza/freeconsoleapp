//
// Created by kaiser on 12/2/22.
//

#include "VideoUserHelp.h"
#include "../../IMenuType.h"

int VideoUserHelp::doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) {
    menuItem->ShowAvailableCommands();
    return 0;
}
