//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"

class InsertVideoUser : public  ICommand{
public:
    InsertVideoUser()
    {
        this->command = "insert";
        this->description = "insert: inert a video game in database"
                            "insert <userId> <videoGameId> <achievementsNumber> <rentalDate> <returnDate>"
                            "insert 1 3 5 22/01/2022 01/02/2022";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override{
        std::cout << "DO InsertVideoUser" << std::endl;
        if (args.size() != 5){
            return 0;
        }
        DatabaseConnector::instance()->insertVideoUser(args.at(0),args.at(1),args.at(2), args.at(3), args.at(4));
        return 5;
    };
};

