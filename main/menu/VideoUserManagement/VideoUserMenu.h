//
// Created by kaiser on 12/2/22.
//

#pragma once
#include "../IMenuType.h"
#include "./Commands/DeleteVideoUser.h"
#include "./Commands/InsertVideoUser.h"
#include "./Commands/UpdateVideoUser.h"
#include "./Commands/VideoUserHelp.h"

//#include "../../App.h"
class App;

class VideoUserMenu : public  IMenuType, public  std::enable_shared_from_this<VideoUserMenu>{
public:
    VideoUserMenu():IMenuType("videousers"){
        description = "videousers: modify videogames and user information in database";
    };

    void showInformation() override {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    void LoadAvailableCommands() override {
        if(!commands.empty()){
            return;
        }
        commands.emplace_back(std::make_shared<InsertVideoUser>());
        commands.emplace_back(std::make_shared<DeleteVideoUser>());
        commands.emplace_back(std::make_shared<VideoUserHelp>());
        commands.emplace_back(std::make_shared<UpdateVideoUser>());

    }

    void ShowAvailableCommands() override
    {
        std::cout << "**** " << command << " *******" << std::endl;
        for (const auto& t : commands)
        {
            std::cout << t->description << std::endl;
        }
        std::cout << "" << std::endl;
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) override{
        std::cout << "DO VideoUserMenu" << std::endl;

        try
        {
            std::vector<std::shared_ptr<IMenuType>> commandsToExecute;
            int position;
            bool toolFound = false;
            for (position = 0; position < args.size(); position++)
            {
                std::string command = args[position];
                auto tool = std::find_if(commands.begin(), commands.end(), [&command](std::shared_ptr<ICommand> const & obj) ->bool{
                    return obj->command == command;
                });

                if (tool != commands.end())
                {
                    args.erase(args.begin());
                    toolFound = true;
                    position += (*tool)->doCommand(args,shared_from_this());
                }
            }
            if (!toolFound)
            {
                std::cout << "Not found .. " << std::endl;
            }

            return position;
        }catch( const std::exception & ex)
        {
            std::cout << "Error .. " <<  ex.what() << std::endl;
        }
        return 0;
    };

};


