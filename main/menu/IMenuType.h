//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <memory>
#include <algorithm>
#include "./ICommand.h"
//#include "../App.h"

class App;

class IMenuType {
public:
    explicit IMenuType(std::string command): command(std::move(command)){};

    std::vector<std::shared_ptr<ICommand>> commands;

    virtual  void showInformation() = 0;
    virtual int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) = 0;
    virtual void ShowAvailableCommands() = 0;
    virtual void LoadAvailableCommands() =0;

    std::string command;
    std::string description;

};


