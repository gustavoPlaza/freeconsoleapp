//
// Created by kaiser on 12/2/22.
//

#pragma once
#include "../IMenuType.h"
#include "Commands/InsertUser.h"
#include "Commands/DeleteUser.h"
#include "Commands/UserHelp.h"
#include "Commands/UpdateUser.h"
#include "Commands/SelectUser.h"
#include <iostream>
#include <memory>
#include <algorithm>
#include "fmt/core.h"

class App;

class UserMenu : public  IMenuType, public  std::enable_shared_from_this<UserMenu>{
public:

    void ShowAvailableCommands() override
    {
        fmt::print(fmt::format("{:*^60}\n", command));
        for (const auto& t : commands)
        {
            fmt::print(fmt::format("- {:<30}\n", t->description));
        }
        std::cout << "" << std::endl;
    }

    void LoadAvailableCommands() override {
        if(!commands.empty()){
            return;
        }
        commands.emplace_back(std::make_shared<InsertUser>());
        commands.emplace_back(std::make_shared<DeleteUser>());
        commands.emplace_back(std::make_shared<UserHelp>());
        commands.emplace_back(std::make_shared<UpdateUser>());
        commands.emplace_back(std::make_shared<SelectUser>());
    }

    UserMenu(): IMenuType("users")
    {
        description = "users: modify users information in database";
    };

    void showInformation() override {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) override{
        fmt::print(fmt::format("- {:<30}\n", "Running a users command"));
        try
        {
            std::vector<std::shared_ptr<IMenuType>> commandsToExecute;
            int position;
            bool toolFound = false;
            for (position = 0; position < args.size(); position++)
            {
                std::string command = args[position];
                auto tool = std::find_if(commands.begin(), commands.end(), [&command](std::shared_ptr<ICommand> const & obj) ->bool{ return obj->command == command; });

                if (tool != commands.end())
                {
                    args.erase(args.begin());
                    toolFound = true;
                    position += (*tool)->doCommand(args,shared_from_this());
                }
            }
            if (!toolFound)
            {
                std::cout << "Not found .. " << std::endl;
            }

            return position;
        }catch( const std::exception & ex)
        {
            std::cout << "Error .. " <<  ex.what() << std::endl;
        }
        return 0;
    };
};


