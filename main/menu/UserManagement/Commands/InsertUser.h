//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"

class InsertUser : public  ICommand{
public:
    InsertUser()
    {
        this->command = "insert";
        this->description = "insert: inert a user in database \n"
                            "  insert <first name> <last name> <birth date> \n"
                            "  insert Gustavo Plaza 22/07/1994 \n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override{
        std::cout << "DO InsertUser" << std::endl;

        if (args.size() != 3){
            return 0;
        }

        DatabaseConnector::instance()->insertUser(args.at(0),args.at(1),args.at(2));

        for (auto & arg: args) {
            std::cout << "arg: " << arg << std::endl;
        }
        return 3;
    };
};

