//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"

class DeleteUser : public  ICommand{
public:
    DeleteUser()
    {
        this->command = "delete";
        this->description = "delete: delete a user in database\n"
                            "  delete <id>\n"
                            "  delete 1\n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO DeleteUser" << std::endl;
        if (args.size() != 1) {
            return 0;
        }
        DatabaseConnector::instance()->deleteUser(args.at(0));

        return 1;
    };
};

