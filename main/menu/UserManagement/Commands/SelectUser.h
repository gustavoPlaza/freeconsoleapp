//
// Created by kaiser on 13/2/22.
//

#pragma once
#include <iostream>
#include <vector>
#include <memory>
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"
#include "../../Helper.h"
#include "../../ICommand.h"

class SelectUser : public  ICommand{
public:
    SelectUser()
    {
        this->command = "select";
        this->description = "select: delete a user in database\n"
                            "  select <id>\n"
                            "  select 1\n"
                            "  select <all>\n"
                            "  select all\n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    static void printVideoGamesInfoByUSerId(const std::string& id){
        try {
            auto user_id = std::stoi(id);
            auto [res, user] = DatabaseConnector::instance()->getUser(id);
            if(res){
                fmt::print(fmt::format(" {:^30} | {:^30} | {:^30}\n", user.firstName, user.lastName, user.birthDate));
                fmt::print(fmt::format(" {:-^90}\n", "-"));
            }
            auto games = DatabaseConnector::instance()->getAllVideoGamesByUser(id);
            for (const auto& game: games) {
                fmt::print(fmt::format(" {:^30} | {:^30} | {:^30}\n", game.name, game.company, game.publishDate));
            }
            fmt::print(fmt::format(" {:-^90}\n", "-"));
        } catch (const std::exception &e) {
            std::cout << "Argument is not an Id" << std::endl;
        }
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO SelectUser" << std::endl;
        if (args.empty()) {
            return 0;
        }
        if(args.at(0)=="all"){
            for (const auto& user: DatabaseConnector::instance()->getAllUsers()) {
                printVideoGamesInfoByUSerId(std::to_string(user.id));
            }
            return 1;
        }
        printVideoGamesInfoByUSerId(args.at(0));
        return 1;
    };
};