//
// Created by kaiser on 13/2/22.
//

#pragma once
#include <iostream>
#include <vector>
#include <memory>
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"
#include "../../Helper.h"
#include "../../ICommand.h"

class UpdateUser : public  ICommand{
public:
    UpdateUser()
    {
        this->command = "update";
        this->description = "update: delete a user in database\n"
                            "  update <id> <property1> = <value1>\n"
                            "  update 1 firstName Test\n"
                            "  update 1 lastName Test2\n"
                            "  update 1 birthDate 15/12/1994\n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO UpdateUser" << std::endl;
        if (args.size() < 2) {
            return 0;
        }
        auto user_id = args.at(0);
        args.erase(args.begin());

        std::ostringstream s;
        copy(args.begin(),args.end(), std::ostream_iterator<std::string>(s,std::string(" ").c_str()));
        std::string  s_ = s.str();
        s_.erase(std::remove(s_.begin(), s_.end(), ' '), s_.end());
        args = Helper::splitString(s_, "=");

        DatabaseConnector::instance()->updateUser(user_id, args.at(0), args.at(1));

        return 1;
    };
};