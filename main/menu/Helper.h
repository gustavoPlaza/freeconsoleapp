//
// Created by kaiser on 13/2/22.
//

#pragma once
#include <string>
#include <vector>

class Helper {
public:
    static std::vector<std::string> splitString(const std::string& str, const std::string& delimiter = " ")
    {
        std::vector<std::string> split_vec;
        unsigned long start = 0;
        unsigned long end = str.find(delimiter);
        while (end != -1) {
            split_vec.emplace_back(str.substr(start, end - start));
            start = end + delimiter.size();
            end = str.find(delimiter, start);
        }
        split_vec.emplace_back(str.substr(start, end - start));

        return split_vec;
    }
};