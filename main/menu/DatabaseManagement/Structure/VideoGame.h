//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>

struct VideoGame
{
    int id;
    std::string name;
    std::string company;
    std::string publishDate;
};
