//
// Created by kaiser on 12/2/22.
//
#pragma once
#include <iostream>

struct Date{
    int month;
    int day;
    int year;
};

struct User
{
public:
    int id;
    std::string firstName;
    std::string lastName;
    std::string birthDate;
};

