//
// Created by kaiser on 12/2/22.
//
#pragma once
#include <iostream>

struct UserVideoGames
{
    int id;
    int userId;
    int videoGameId;
    std::string achievementsNumber;
    std::string rentalDate;
    std::string returnDate;
};