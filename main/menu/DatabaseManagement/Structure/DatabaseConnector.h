//
// Created by kaiser on 13/2/22.
//

#pragma once

#include <sqlite_orm/sqlite_orm.h>
#include <string>
#include "User.h"
#include "VideoGame.h"
#include "userVideoGame.h"
#include <regex>
#include <utility>

/*using namespace sqlite_orm;
template<class O, class T, class ...Op>
using Column = internal::column_t<O, T, const T& (O::*)() const, void (O::*)(T), Op...>;
using Storage2 = internal::storage_t<
        sqlite_orm::internal::table_t<User,
                Column<User, decltype(User::id), internal::autoincrement_t, internal::primary_key_t<>>,
                Column<User, decltype(User::firstName)>,
                Column<User, decltype(User::lastName)>,
                Column<User, decltype(User::birthDate)>>,
        sqlite_orm::internal::table_t<VideoGame,
                Column<VideoGame, decltype(VideoGame::id), internal::autoincrement_t, internal::primary_key_t<>>,
                Column<VideoGame, decltype(VideoGame::name)>,
                Column<VideoGame, decltype(VideoGame::company)>,
                Column<VideoGame, decltype(VideoGame::publishDate)>>,
        sqlite_orm::internal::table_t<UserVideoGames,
                Column<UserVideoGames, decltype(UserVideoGames::userId)>,
                Column<UserVideoGames, decltype(UserVideoGames::videoGameId)>,
                Column<UserVideoGames, decltype(UserVideoGames::achievementsNumber)>,
                Column<UserVideoGames, decltype(UserVideoGames::returnDate)>,
                Column<UserVideoGames, decltype(UserVideoGames::rentalDate)>>
>;
*/
inline auto  make_storage_query(const std::string & path) {
    using namespace sqlite_orm;
    return  make_storage(path,
                        make_table("user",
                                   make_column("id", &User::id, autoincrement(), primary_key()),
                                   make_column("first_name", &User::firstName),
                                   make_column("last_name", &User::lastName),
                                   make_column("type_id", &User::birthDate)),
                        make_table("videoGame",
                                   make_column("id", &VideoGame::id, autoincrement(), primary_key()),
                                   make_column("name", &VideoGame::name),
                                   make_column("company", &VideoGame::company),
                                   make_column("publishDate", &VideoGame::publishDate)),
                        make_table("userVideoGame",
                                   make_column("userId", &UserVideoGames::userId),
                                   make_column("videoGameId", &UserVideoGames::videoGameId),
                                   make_column("achievementsNumber", &UserVideoGames::achievementsNumber),
                                   make_column("returnDate", &UserVideoGames::returnDate),
                                   make_column("rentalDate", &UserVideoGames::rentalDate)));
}


class DatabaseConnector {
public:
    using Storage = decltype(make_storage_query(""));
    std::shared_ptr<Storage> storage;
    std::map<std::string, std::shared_ptr<Storage>> storages;

    static bool isNumber(const std::string & s)
    {
        return (std::all_of(s.cbegin(), s.cend(), [](char const &ch){ return std::isdigit(ch); }));
    }

    static std::tuple<bool, Date>  dateValidation(const std::string& data){
        std::match_results<std::string::const_iterator> m;
        Date date{};
        std::regex re(R"(\b(\d{2})[/](\d{2})[/](\d{4})\b)");
        if (regex_match(data, m, re)) {
            date.day = stoi(m[1].str()),
            date.month = stoi(m[2].str()),
            date.year = stoi(m[3].str());
            return {true,date};
        }
        else {
            std::cout << "Invalid date time" << std::endl;
            date.year = 0000;
            return {false,date};
        }
    }


    void createdb(const std::string& path){
        storage = std::make_shared<Storage>(make_storage_query(path));
        storage->sync_schema();
    }

    void insertUser(std::string first_name, std::string last_name, const std::string& birth_date){
        auto [result, dateTime] = dateValidation(birth_date);
        if(result){
            if(storage) {
                storage->insert(User{0, std::move(first_name), std::move(last_name), birth_date});
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }
    void insertVideoUser(const std::string& userId, const std::string& gameId, const std::string& achievementsNumber,
                         const std::string& rentalDate, const std::string&  returnDate){
        auto [result2, dateTime2] = dateValidation(rentalDate);
        auto [result3, dateTime3] = dateValidation(returnDate);
        if(result2 && result3){
            if(storage) {
                storage->insert(UserVideoGames{0, std::stoi(userId), std::stoi(gameId),
                                               achievementsNumber, rentalDate, returnDate});
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }
    void insertVideoGame(std::string name, std::string company, const std::string& publish_date){
        auto [result, dateTime] = dateValidation(publish_date);
        if(result){
            if(storage) {
                storage->insert(VideoGame{0, std::move(name), std::move(company), publish_date});
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }
    void deleteUser(const std::string& id){
        if(isNumber(id)){
            if(storage) {
                storage->remove<User>(id);
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }

    void deleteVideoGame(const std::string& id){
        if(isNumber(id)){
            if(storage) {
                storage->remove<VideoGame>(id);
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }
    void deleteVideoUser(const std::string& id){
        if(isNumber(id)){
            if(storage) {
                storage->remove<UserVideoGames>(id);
            } else {
                std::cout << "Database is not ready" << std::endl;
            }
        }
    }

    bool cleanDatabase(){
        if(storage){
            storage->remove_all<User>();
            storage->remove_all<VideoGame>();
            storage->remove_all<UserVideoGames>();
            return true;
        }else{
            return false;
        }

    }

    void removeDatabase(){
        if(storage){
            storage->remove_all<User>();
            storage->remove_all<VideoGame>();
            storage->remove_all<UserVideoGames>();
        }
    }

    std::tuple<bool, User> getUser(std::string id){
        try {
            if(storage) {
                auto user = storage->get<User>(std::move(id));
                return  {true,user};
            } else {
                std::cout << "Database is not ready" << std::endl;
                return  {false,User{}};
            }
        }
        catch (const std::system_error &e) {
            std::cout << e.what() << std::endl;
            return  {false,User{}};
        }
    }
    std::tuple<bool, VideoGame> getVideoGame(std::string id){
        try {
            if(storage) {
                auto video_game = storage->get<VideoGame>(std::move(id));
                return  {true,video_game};
            } else {
                std::cout << "Database is not ready" << std::endl;
                return  {false,VideoGame{}};
            }
        }
        catch (const std::system_error &e) {
            std::cout << e.what() << std::endl;
            return  {false,VideoGame{}};
        }
    }
    std::tuple<bool, UserVideoGames> getVideoUser(std::string id){
        try {
            if(storage) {
                auto video_user = storage->get<UserVideoGames>(std::move(id));
                return  {true,video_user};
            } else {
                std::cout << "Database is not ready" << std::endl;
                return  {false,UserVideoGames{}};
            }
        }
        catch (const std::system_error &e) {
            std::cout << e.what() << std::endl;
            return  {false,UserVideoGames{}};
        }
    }

    std::vector<VideoGame> getAllVideoGamesByUser(std::string userId){
        using namespace sqlite_orm;
        std::vector<VideoGame> videoGames;
        std::vector<UserVideoGames>  videogamesbyuser;
        if(storage) {
            videogamesbyuser = storage->get_all<UserVideoGames>(where(c(&UserVideoGames::userId) == std::move(userId)));
        } else {
            std::cout << "Database is not ready" << std::endl;
            return videoGames;
        }
        for (const auto& videouser: videogamesbyuser) {
            auto [res, vide_game] = getVideoGame(std::to_string(videouser.videoGameId));
            if(res){
                videoGames.emplace_back(vide_game);
            }
        }
        return videoGames;
    }

    std::vector<User> getAllUsers(){
        return storage->get_all<User>();
    }

    void updateUser(std::string id, const std::string& property, const std::string& value){
        auto [res, user] = getUser(std::move(id));
        if(!res) {
            return;
        }
        if(property == "firstName"){
            user.firstName = value;
        }else if(property == "lastName"){
            user.lastName = value;
        }else if(property == "birthDate"){
            user.birthDate = value;
        }else{
            return;
        }
        storage->replace(user);
    }

    void updateVideoGame(std::string id, const std::string& property, const std::string& value){
        auto [res, video_game] = getVideoGame(std::move(id));
        if(!res) {
            return;
        }
        if(property == "name"){
            video_game.name = value;
        }else if(property == "company"){
            video_game.company = value;
        }else if(property == "publishDate"){
            video_game.publishDate = value;
        }else{
            return;
        }
        storage->replace(video_game);
    }

    void updateVideoUser(std::string id, const std::string& property, const std::string& value){
        auto [res, video_user] = getVideoUser(std::move(id));
        if(!res) {
            return;
        }
        if(property == "userId"){
            video_user.userId = std::stoi(value);
        }else if(property == "gameId"){
            video_user.videoGameId = std::stoi(value);
        }else if(property == "achievementsNumber"){
            video_user.achievementsNumber = value;
        }else if(property == "rentalDate"){
            video_user.rentalDate = value;
        }else if(property == "returnDate"){
            video_user.returnDate = value;
        }else{
            return;
        }

        storage->replace(video_user);
    }


public:
    DatabaseConnector(const DatabaseConnector&) = delete;
    DatabaseConnector& operator=(const DatabaseConnector &) = delete;
    DatabaseConnector(DatabaseConnector &&) = delete;
    DatabaseConnector & operator=(DatabaseConnector &&) = delete;

    static std::shared_ptr<DatabaseConnector> instance(){
        static std::shared_ptr<DatabaseConnector> d{new DatabaseConnector};
        return d;
    }

protected:
    DatabaseConnector()= default;
};


