//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include <memory>
#include <vector>
class RemoveDatabase : public  ICommand{
public:
    RemoveDatabase()
    {
        this->command = "remove";
        this->description = "remove: remove complete selected database\n";
    };

    static void showInformation() {
        std::cout << "RemoveDatabase" << std::endl;
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO Remove database" << std::endl;
        if (!args.empty()) {
            return 0;
        }
        DatabaseConnector::instance()->removeDatabase();
        return 0;
    };
};

