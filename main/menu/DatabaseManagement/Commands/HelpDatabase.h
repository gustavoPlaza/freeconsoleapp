//
// Created by kaiser on 12/2/22.
//

#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include "../../ICommand.h"

class HelpDatabase : public  ICommand{
public:
    HelpDatabase()
    {
        this->command = "help";
        this->description = "help: show database options\n";
    };

    virtual void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override;
};
