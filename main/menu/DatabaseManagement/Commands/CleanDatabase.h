//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include <memory>
#include <vector>
class CleanDatabase : public  ICommand{
public:
    CleanDatabase()
    {
        this->command = "clean";
        this->description = "\rclean: remove all items from selected database\n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    virtual bool  cleanDatabase(){
        return DatabaseConnector::instance()->cleanDatabase();
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO Clean database" << std::endl;
        if (!args.empty()) {
            return -1;
        }
        if(!cleanDatabase()) {
            return -1;
        }
        return 0;
    };
};

