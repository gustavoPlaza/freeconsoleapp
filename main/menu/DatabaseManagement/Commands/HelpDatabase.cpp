//
// Created by kaiser on 12/2/22.
//

#include "HelpDatabase.h"
#include "../../IMenuType.h"

int HelpDatabase::doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) {
    menuItem->ShowAvailableCommands();
    return 0;
}
