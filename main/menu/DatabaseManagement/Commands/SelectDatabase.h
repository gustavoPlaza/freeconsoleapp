//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include <memory>
#include <vector>
#include "../../ICommand.h"

class SelectDatabase : public  ICommand{
public:
    SelectDatabase()
    {
        this->command = "select";
        this->description = "select <path to database>: select database from path\n";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {

        if(args.size()>1 || args.empty()){
            showInformation();
            return 0;
        }

        std::filesystem::path database_path;

        if(args.at(0) == "__default__"){
            std::cout << "Current working directory: " << std::filesystem::current_path()  << std::endl;
            database_path = std::filesystem::current_path();
        } else{
            database_path = std::filesystem::path(args.at(0));
        }

        if(!std::filesystem::exists(std::filesystem::path(database_path/"my_database.db"))){
            fmt::print("Invalid path: {}\n", std::filesystem::path(database_path/"my_database.db").string());
            return 0;
        }

        DatabaseConnector::instance()->createdb(std::filesystem::path(database_path/"my_database.db").string());

        return 1;
    };
};

