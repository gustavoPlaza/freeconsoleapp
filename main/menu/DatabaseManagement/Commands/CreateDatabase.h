//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include <memory>
#include <vector>
#include <filesystem>
#include "fmt/core.h"
#include "../Structure/DatabaseConnector.h"

class CreateDatabase : public  ICommand{
public:
    CreateDatabase()
    {
        this->command = "create";
        this->description = "create <path to database>: create database in a specific folder\n"
                            "If \"create __default__\" is used, database is created in current path\n";
    };

    void showInformation() {
        std::cout << this->description << std::endl;
    };

    virtual void createDatabase(const std::filesystem::path & database_path){
        DatabaseConnector::instance()->createdb(std::filesystem::path(database_path/"my_database.db").string());
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {

        if(args.size()>1 || args.empty()){
            showInformation();
            return 0;
        }
        std::filesystem::path database_path;

        if(args.at(0) == "__default__"){
            std::cout << "Current working directory: " << std::filesystem::current_path()  << std::endl;
            database_path = std::filesystem::current_path();
        } else{
            database_path = std::filesystem::path(args.at(0));
        }

        if(!std::filesystem::is_directory(database_path)){
            fmt::print("Invalid path: {}", database_path.string());
            return 0;
        }

        createDatabase(database_path);
        return 1;
    };
};

