//
// Created by kaiser on 12/2/22.
//

#pragma once
#include "../IMenuType.h"
#include "Commands/CreateDatabase.h"
#include "Commands/RemoveDatabase.h"
#include "Commands/CleanDatabase.h"
#include "Commands/SelectDatabase.h"
#include "Commands/HelpDatabase.h"
#include <iostream>
#include <memory>
#include <algorithm>
//#include "../../App.h"

class App;

class DatabaseMenu : public  IMenuType, public  std::enable_shared_from_this<DatabaseMenu>{
public:

    void ShowAvailableCommands() override
    {
        fmt::print(fmt::format("{:*^60}\n", command));
        for (const auto& t : commands)
        {
            fmt::print(fmt::format("- {:<30}\n", t->description));
        }
        std::cout << "" << std::endl;
    }

    void LoadAvailableCommands() override {
        if(!commands.empty()){
            return;
        }
        commands.emplace_back(std::make_shared<CleanDatabase>());
        commands.emplace_back(std::make_shared<CreateDatabase>());
        commands.emplace_back(std::make_shared<HelpDatabase>());
        commands.emplace_back(std::make_shared<RemoveDatabase>());
        commands.emplace_back(std::make_shared<SelectDatabase>());
    }

    DatabaseMenu(): IMenuType("database")
    {
        description = "database: modify database";
        //LoadAvailableCommands();
    };

    void showInformation() override {
        fmt::print(fmt::format("- {:<30}\n", description));
    };


    int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) override{
        std::cout << "DO DATABASE MENU" << std::endl;
        try
        {
            std::vector<std::shared_ptr<IMenuType>> commandsToExecute;
            int position;
            bool toolFound = false;
            for (position = 0; position < args.size(); position++)
            {
                std::string command = args[position];
                auto tool = std::find_if(commands.begin(), commands.end(), [&command](std::shared_ptr<ICommand> const & obj) ->bool{ return obj->command == command; });

                if (tool != commands.end())
                {
                    args.erase(args.begin());
                    toolFound = true;
                    position+=(*tool)->doCommand(args,shared_from_this());
                }
            }
            if (!toolFound)
            {
                std::cout << "Not found .. " << std::endl;
            }
            return position;
        }catch( const std::exception & ex)
        {
            std::cout << "Error .. " <<  ex.what() << std::endl;
        }

        return 0;

    };

};


