//
// Created by kaiser on 12/2/22.
//

#pragma once
#include "../IMenuType.h"
#include "./Commands/DeleteVideogame.h"
#include "./Commands/InsertVideoGame.h"
#include "./Commands/UpdateVideoGame.h"
#include "./Commands/VideoGameHelp.h"

//#include "../../App.h"
class App;

class VideoGameMenu : public  IMenuType, public  std::enable_shared_from_this<VideoGameMenu>{
public:
    VideoGameMenu():IMenuType("videogames"){
        description = "videogames: modify videogames information in database";
        //LoadAvailableCommands();
    };

    void showInformation() override {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    void LoadAvailableCommands() override {
        if(!commands.empty()){
            return;
        }
        commands.emplace_back(std::make_shared<InsertVideoGame>());
        commands.emplace_back(std::make_shared<DeleteVideoGame>());
        commands.emplace_back(std::make_shared<VideoGameHelp>());
        commands.emplace_back(std::make_shared<UpdateVideoGame>());
    }

    void ShowAvailableCommands() override
    {
        std::cout << "**** " << command << " *******" << std::endl;
        for (const auto& t : commands)
        {
            std::cout << t->description << std::endl;
        }
        std::cout << "" << std::endl;
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) override{
        std::cout << "DO VideoGameMenu" << std::endl;

        try
        {
            std::vector<std::shared_ptr<IMenuType>> commandsToExecute;
            int position;
            bool toolFound = false;
            for (position = 0; position < args.size(); position++)
            {
                std::string command = args[position];
                auto tool = std::find_if(commands.begin(), commands.end(), [&command](std::shared_ptr<ICommand> const & obj) ->bool{
                    return obj->command == command;
                });

                if (tool != commands.end())
                {
                    args.erase(args.begin());
                    toolFound = true;
                    position += (*tool)->doCommand(args,shared_from_this());
                }
            }
            if (!toolFound)
            {
                std::cout << "Not found .. " << std::endl;
            }

            return position;
        }catch( const std::exception & ex)
        {
            std::cout << "Error .. " <<  ex.what() << std::endl;
        }
        return 0;
    };

};


