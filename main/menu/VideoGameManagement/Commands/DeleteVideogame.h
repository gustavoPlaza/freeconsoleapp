//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"

class DeleteVideoGame : public  ICommand{
public:
    DeleteVideoGame()
    {
        this->command = "delete";
        this->description = "delete: delete a video game in database"
                            "delete <id>"
                            "delete 1";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO DeleteVideoGame" << std::endl;
        if (args.size() != 1) {
            return 0;
        }
        DatabaseConnector::instance()->deleteVideoGame(args.at(0));

        return 1;
    };
};

