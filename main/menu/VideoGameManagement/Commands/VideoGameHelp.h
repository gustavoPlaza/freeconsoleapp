//
// Created by kaiser on 12/2/22.
//

#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include "../../ICommand.h"

class VideoGameHelp : public  ICommand{
public:
    VideoGameHelp()
    {
        this->command = "help";
        this->description = "help: show this help";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override;
};
