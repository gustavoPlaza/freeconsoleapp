//
// Created by kaiser on 13/2/22.
//

#pragma once
#include <iostream>
#include <vector>
#include <memory>
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"
#include "../../Helper.h"
#include "../../ICommand.h"

class UpdateVideoGame : public  ICommand{
public:
    UpdateVideoGame()
    {
        this->command = "update";
        this->description = "update: delete a video game in database"
                            "update <id> <property1> = <value1>"
                            "update 1 name Test"
                            "update 1 company Test2"
                            "update 1 publishDate 15/12/1994";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override {
        std::cout << "DO UpdateUser" << std::endl;
        if (args.size() < 2) {
            return 0;
        }
        auto user_id = args.at(0);
        args.erase(args.begin());

        std::ostringstream s;
        copy(args.begin(),args.end(), std::ostream_iterator<std::string>(s,std::string(" ").c_str()));
        std::string  s_ = s.str();
        s_.erase(std::remove(s_.begin(), s_.end(), ' '), s_.end());
        args = Helper::splitString(s_, "=");

        DatabaseConnector::instance()->updateVideoGame(user_id, args.at(0), args.at(1));

        return 1;
    };
};