//
// Created by kaiser on 12/2/22.
//

#pragma once
#include <iostream>
#include "../../ICommand.h"
#include "../../DatabaseManagement/Structure/DatabaseConnector.h"

class InsertVideoGame : public  ICommand{
public:
    InsertVideoGame()
    {
        this->command = "insert";
        this->description = "insert: inert a video game in database"
                            "insert <name> <company> <publish date>"
                            "insert FIFA20 EASports 01/01/2020";
    };

    void showInformation() {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) override{
        std::cout << "DO InsertUser" << std::endl;

        if (args.size() != 3){
            return 0;
        }

        DatabaseConnector::instance()->insertVideoGame(args.at(0),args.at(1),args.at(2));

        return 3;
    };
};

