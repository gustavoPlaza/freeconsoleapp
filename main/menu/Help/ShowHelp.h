//
// Created by kaiser on 12/2/22.
//

#pragma once
#include "../IMenuType.h"
//#include "../../App.h"
#include <iostream>
#include <memory>
#include <algorithm>
#include "fmt/core.h"

class App;

class ShowHelp : public  IMenuType, public  std::enable_shared_from_this<ShowHelp>{
public:


    ShowHelp(): IMenuType("help")
    {
        description = "help: show application help";
    };
    void ShowAvailableCommands() override {};

    void showInformation() override {
        fmt::print(fmt::format("- {:<30}\n", description));
    };

    void LoadAvailableCommands() override {
    }

    int doCommand(std::vector<std::string> args, std::shared_ptr<App> app) override;
};
