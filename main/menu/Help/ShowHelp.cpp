//
// Created by kaiser on 12/2/22.
//


#include "ShowHelp.h"

#include "../../App.h"

int ShowHelp::doCommand(std::vector<std::string> args, std::shared_ptr<App> app) {
    app->ShowAvailableTools();
    return 0;
}

