//
// Created by kaiser on 12/2/22.
//


#pragma once
#include <iostream>
#include <string>
//#include "./IMenuType.h"
#include "fmt/core.h"

class IMenuType;

class ICommand {
public:
    ICommand()= default;;
    std::string command;
    std::string description;

    virtual  int doCommand(std::vector<std::string> args, std::shared_ptr<IMenuType> menuItem) = 0;

};


