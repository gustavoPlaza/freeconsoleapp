#include "../../main/menu/DatabaseManagement/DatabaseMenu.h"
#include "testcleandatabase.h"

using ::testing::Return;

TEST_F(CleanDatabaseTest, BadArgumentNumber) {
    ON_CALL(m_clean,cleanDatabase()).WillByDefault(Return(true));
    EXPECT_CALL(m_clean,cleanDatabase()).Times(0);
    EXPECT_EQ(m_clean.doCommand({"arg1","arg2"}, std::make_shared<DatabaseMenu>()), -1);
}

TEST_F(CleanDatabaseTest, BadCleanDatabase) {
    ON_CALL(m_clean,cleanDatabase()).WillByDefault(Return(false));
    EXPECT_CALL(m_clean,cleanDatabase()).Times(1);
    EXPECT_EQ(m_clean.doCommand({}, std::make_shared<DatabaseMenu>()), -1);
}

TEST_F(CleanDatabaseTest, CorrectClean) {
    ON_CALL(m_clean,cleanDatabase()).WillByDefault(Return(true));
    EXPECT_CALL(m_clean,cleanDatabase()).Times(1);
    EXPECT_EQ(m_clean.doCommand({}, std::make_shared<DatabaseMenu>()), 0);
}

