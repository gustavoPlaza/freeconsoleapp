#include "../../main/menu/DatabaseManagement/DatabaseMenu.h"
#include "testcreatedatabase.h"

using ::testing::Return;

TEST_F(CreateDatabaseTest, DefaultArgument) {
    EXPECT_CALL(m_create,createDatabase(::testing::_)).Times(1);
    EXPECT_EQ(m_create.doCommand({"__default__"}, std::make_shared<DatabaseMenu>()), 1);
}

TEST_F(CreateDatabaseTest, InvalidPath) {
    EXPECT_CALL(m_create,createDatabase(::testing::_)).Times(0);
    EXPECT_EQ(m_create.doCommand({"/path/not/exist"}, std::make_shared<DatabaseMenu>()), 0);
}

TEST_F(CreateDatabaseTest, NoDatabasePath) {
    EXPECT_CALL(m_create,createDatabase(::testing::_)).Times(0);
    EXPECT_EQ(m_create.doCommand({}, std::make_shared<DatabaseMenu>()), 0);
}

