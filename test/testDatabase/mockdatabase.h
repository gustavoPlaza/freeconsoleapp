#include "gmock/gmock.h"
#include "../../main/menu/DatabaseManagement/Commands/CleanDatabase.h"
#include "../../main/menu/DatabaseManagement/Commands/CreateDatabase.h"
#include <filesystem>

class MockCleanDatabase: public CleanDatabase
{
public:
    MOCK_METHOD0(cleanDatabase, bool());
};

class MockCreateDatabase: public CreateDatabase
{
public:
    MOCK_METHOD1(createDatabase, void(const std::filesystem::path & database_path));
};
