#include "gtest/gtest.h"
#include "mockdatabase.h"

class CleanDatabaseTest : public ::testing::Test {

protected:
    CleanDatabaseTest()= default;;
    ~CleanDatabaseTest() override= default;;
    void SetUp() override{};
    void TearDown() override{};
    MockCleanDatabase m_clean;
};
