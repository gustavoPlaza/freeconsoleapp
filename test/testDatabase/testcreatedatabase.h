#include "gtest/gtest.h"
#include "mockdatabase.h"

class CreateDatabaseTest : public ::testing::Test {

protected:
    CreateDatabaseTest(){
        ON_CALL(m_create,createDatabase).WillByDefault([](const std::filesystem::path& database_path){return;});
    };

    ~CreateDatabaseTest() override= default;
    void SetUp()override{};
    void TearDown()override{};
    MockCreateDatabase m_create;
};
