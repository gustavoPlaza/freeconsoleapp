This small app illustrates how to operate over a database using command line. The application use [Cmake](https://cmake.org/) as build tool and [Conan](https://conan.io/) to import dependencies.

The application emulates a "video game rental store", the user can store users, video games and its relationship. A user can rent multiple games and a game can be rented by multiple users. Database structures is as follows:

![Database](doc/database.png)


The application has been developed and tested under Linux (gcc 9.3 and cmake 3.16)

## Dependencies 

Using Conan, the project will download automatically the following libraries:

~~~~~~~
gtest 1.10.0 
fmt 8.1.1
~~~~~~~


The project will also download the library [sqlite_orm](https://github.com/fnc12/sqlite_orm) but in this case the library will be download using the cmake module [FetchContent](https://cmake.org/cmake/help/latest/module/FetchContent.html). 

## Building 

~~~
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
~~~

## Running tests

~~~
cd build && make test
~~~

or

~~~~~~~
cd build && ./bin/testMenuApp --gtest_filter=* --gtest_color=yes
~~~~~~~

## Usage

After compiling the application, it can be executed in "interactive mode" where the application will be stopped until the user stop it  or "script mode" where the app start and stop automatically.

*In the folder `db_demo` we can find a demo database that can be used with the application.

### Interactive mode

When the app is executed without arguments the output is:

![Main](doc/images/main_inter.png)

Now the user can write any options in the console, by example, typing "users help" we will get:

![Main](doc/images/main_help_users.png)

Before working with database we need to start the connection, we can write the command:

~~~
database create __default__
~~~

The previous command will create the database is current path, but we could use also run:

~~~
database create /home/user1/
~~~

After create the database or select it if it already exists we can create a user:
~~~
users insert Gustavo Plaza 22/07/1994
~~~

We can add a video game using:
~~~
videogames insert FIFA22 EASports 23/09/2021
videogames insert CallOfDutty Activision 16/09/2021
~~~

Finally, we can add a relation between the user and the game:
~~~
videousers insert 1 1 5 22/01/2022 01/02/2022
~~~

And we can list the stored information:
~~~
users select 1
~~~

The output will be:

![List](doc/images/select_1.png)

### Script mode

In order to run the application in "script mode" we need to pass options as arguments separated by semicolon (";"), by example:

~~~~~~~
./menuApp  "database select __default__; users select 1;"
~~~~~~~

*Note that argument must be written between "".

Output:

![List](doc/images/select_1_2.png)

